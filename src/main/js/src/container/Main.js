import React, { Component } from 'react';
import URL from 'url-parse';
import Entry from './Entry';
import queryString from 'query-string';

import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

class Main extends Component {
	constructor(props) {
	    //Change this as soon as you can
		super(props);
		this.state = {
		    entries: [],
		    state_names: ["*","COLORADO","CONNECTICUT","DELAWARE","FLORIDA","GEORGIA",
            "ALABAMA","ALASKA","ARIZONA","ARKANSAS","CALIFORNIA",
            "KANSAS","IOWA","INDIANA","ILLINOIS","IDAHO","HAWAII",
            "MICHIGAN","MASSACHUSETTS","MAINE","LOUISIANA","KENTUCKY",
            "NEBRASKA","MONTANA","MISSOURI","MISSISSIPPI","MINNESOTA",
            "NEW-YORK","NEW-MEXICO","NEW-JERSEY","NEW-HAMPSHIRE","NEVADA",
            "NORTH-CAROLINA","NORTH-DAKOTA","OHIO","OKLAHOMA",
            "OREGON","PENNSYLVANIA","SOUTH-CAROLINA","SOUTH-DAKOTA","TENNESSEE",
            "TEXAS","UTAH","VERMONT", "WASHINGTON", "WEST-VIRGINIA",
            "WISCONSIN","WYOMING","MARYLAND","RHODE-ISLAND","VIRGINIA"],
            type: ["*","Bills Only","Articles Only"],
            stateSelect: '*',
            typeSelect: '*'
		};
        this.changeStateView = this.changeStateView.bind(this);
	}

	componentWillMount() {
            fetch('http://localhost:8080/entries/*',
                {
                    method: 'GET',
                    headers: {'Allow-Control-Origin': '*'},
                    mode: 'cors',
                    cache: 'default'
                })
                .then(response => response.json())      //Transfers all data in as a string
                .then(data => {
                    this.setState({entries: data});     //Transfers all data within states
                });
        //this.setState({oldEntries:this.state.entries});
	}
    componentWillUpdate(nextProps,nextState) {
        console.log("Component will receive props",nextProps,nextState);
        console.log("nextState.stateSelect: "+nextState.stateSelect);
        console.log("thisState.stateSelect: "+this.state.stateSelect);
        console.log('http://localhost:8080/entries/'+nextState.stateSelect);
        //
        console.log('this.state.entries: '+this.state.entries.length);
        console.log('nextState.entries: '+nextState.entries.length);
        if (nextState.stateSelect !== this.state.stateSelect) {
            console.log("Entered into change state!");
            fetch('http://localhost:8080/entries/'+nextState.stateSelect,
                {
                    method: 'GET',
                    headers: {'Allow-Control-Origin': '*'},
                    mode: 'cors',
                    cache: 'default'
                })
                .then(response => response.json())      //Transfers all data in as a string
                .then(data => {
                    this.setState({entries: data});     //Transfers all data within states
                });
        }
        console.log('Finished nextState.entries: '+nextState.entries.length);
        console.log('Finished this.state.entries: '+this.state.entries.length);
    }

    changeStateView(selectChoice) {
        this.setState({stateSelect: selectChoice.label});
    }

    changeTypeView(selectType) {
        this.setState({typeSelect: selectType.label});
    }

    render() {
        let entryList = this.state.entries.map((ent,index) =><Entry key={index} entry={ent}/>);
		return(
		    <div>
		        <div className = "titleContent">
		            <h1>Knolaw</h1>
		            <h3>Know whats going on in your state</h3>
		        </div>
		            <b className="dropDownMods_text">States Available</b>
                    <b className="dropDownMods_text_filter">Filter By Type</b>
                    <Dropdown className="dropDownMods"
                        options={this.state.state_names}
                        onChange={this.changeStateView}
                        value={this.state.stateSelect} placeHolder="*"/>
		            <Dropdown className="dropDownMods_filter"
		                options={this.state.type}
		                onChange={this.changeTypeView}
		                value={this.state.typeSelect} placeHolder="*"/>
		        <div>{entryList}</div>
		    </div>
		)
	}
}

export default Main;
