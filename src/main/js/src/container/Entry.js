import React, {Component} from 'react';
class Entry extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entry: {
                title:"",
                description:"",
                state:"",
                url:"",
                status:""
            }
        }
        this.goToLink = this.goToLink.bind(this);
    }
    goToLink() {
        let newTab = window.open(this.props.entry.url, '_blank');
        newTab.focus();
    }
    retShortEvent() {
        return(
            <div>
                <div className="EntryBox">
                    <div onClick={this.goToLink}><b>{this.props.entry.title}</b></div>
                    <div className="EntryContent">
                        <b>Place of Origin: </b>{this.props.entry.state}
                        <br></br>
                        <b>Website Obtained From: </b>{this.props.entry.url}
                    </div>
                    <br></br>
                </div>
                <br className="moreSpace"></br>
            </div>
        );
    }
    retEvent() {
        return(
            <div>
                <div className="EntryBox">
                    <div onClick={this.goToLink}><b>{this.props.entry.title}</b></div>
                    <div className="EntryContent">
                        <b>Description: </b>{this.props.entry.description}
                        <br></br>
                        <b>Place of Origin: </b>{this.props.entry.state}
                        <br></br>
                        <b>Website Obtained From: </b>{this.props.entry.url}
                    </div>
                    <br></br>
                </div>
                <br className="moreSpace"></br>
            </div>
            );
    }
    retEventLaw() {
        return(
            <div>
                <div className="EntryBoxLaw">
                    <div onClick={this.goToLink}><b>{this.props.entry.title}</b></div>
                    <div className="EntryContent">
                        <b>Description: </b>{this.props.entry.description}
                        <br></br>
                        <b>Place of Origin: </b>{this.props.entry.state}
                        <br></br>
                        <b>Website Obtained From: </b>{this.props.entry.url}
                        <br></br>
                        <b>Status: </b>{this.props.entry.status}
                    </div>
                    <br></br>
                </div>
                <br className="moreSpace"></br>
            </div>
            );
    }
    render() {
        if (this.props.entry.status !== "Not a Law") {
            return(this.retEventLaw());
        }
        if (this.props.entry.description === "No Description Found") {
            return(this.retShortEvent());
        }
        return (this.retEvent());
    }
}
export default Entry;