package com.knolaw.knolawEntries;

import com.knolaw.WebScrapping.WebScrape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EntryDatabaseList implements CommandLineRunner {
    static EntryRepo repo;
    @Autowired
    public EntryDatabaseList(EntryRepo repository){
        this.repo = repository;
    }

    @Override
    public void run(String... strings) throws Exception {
        List<Entry> storeCurrentSites = WebScrape.scrapeEntries();
        for(Entry ent : storeCurrentSites) {
            //System.out.println(ent);
            if (repo.findEntryByURL(ent.getURL()) != null) {continue;}
            this.repo.save(ent);
        }
        //this.repo.save(new Entry());
    }
}
