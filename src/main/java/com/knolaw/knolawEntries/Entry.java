package com.knolaw.knolawEntries;

import lombok.Data;

import javax.persistence.*;

/*import javax.persistence.OneToMany;
import java.util.List;*/

@Data
@Entity
public class Entry {
    @Id @Column(name="entryId")
    @GeneratedValue(strategy = GenerationType.IDENTITY) private Long entryId;
    private String title, description, state, url, status;
    private boolean isBill;
    /*@OneToMany(cascade=ALL, mappedBy="entry")
    private List<Entry> relatedArticles;*/
    public Entry(){
        this.title = "TEST TITLE";
        this.description = "TEST DESC";
        this.state = "VA";
        this.url = "www.google.com";
        this.isBill = false;
        this.status = "";
    }

    public Entry(String title,
                 String description,
                 String state,
                 String url,
                 boolean isLaw,
                 String status){
        this.title = title;
        this.description = description;
        this.state = state;
        this.url = url;
        this.isBill = isLaw;
        this.status = status;
    }

    public String getTitle() {return title;}
    public String getDescription() {return description;}
    public String getState() {return state;}
    public String getURL() {return url;}

    @Override
    public String toString() {
        String identifyString = "id: "+ this.entryId
                +"\ntitle: "+this.title
                +"\ndesc: "+this.description
                +"\nstate: "+this.state
                +"\nurl: "+this.url;
        identifyString += (this.isBill) ? this.status : "";
        return identifyString;
    }
}
