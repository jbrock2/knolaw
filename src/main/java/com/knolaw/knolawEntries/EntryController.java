package com.knolaw.knolawEntries;

import com.knolaw.WebScrapping.WebScrape;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import sun.misc.Request;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins="*")   //Sets up CORS; server will deny if the referer is something other than itself
public class EntryController {
    //MAPPING
    /*RequestMapping(value="/entries", method=RequestMethod.DELETE)
     public void deleteEntry(Entry selected) {

     }
     */

    @RequestMapping(value="/entries/?reset")
    public void replaceAllEntries() {
        List<Entry> storeCurrentSites = WebScrape.scrapeEntries();
        EntryDatabaseList.repo.deleteAllInBatch();
        EntryDatabaseList.repo.deleteAll();
        for (Entry ent : storeCurrentSites) {
            //System.out.println(ent);
            if (EntryDatabaseList.repo.findEntryByURL(ent.getURL()) != null) {
                continue;
            }
            EntryDatabaseList.repo.save(ent);
        }
    }

    @RequestMapping(value="/entries/*")
    public List<Entry> allEntries() {
        return entry();
    }
    @RequestMapping(value="/entries/*/*")
    public List<Entry> allBillArtEntries() {
        return entry();
    }

    @RequestMapping(value="/entries/*/{isBill}")
    public List<Entry> allBillEntries(@PathVariable("isBill") boolean isBill) {
        List<Entry> tE = EntryDatabaseList.repo.findByOnlyType(isBill);
        return tE;
    }
    @RequestMapping(value="/entries/{state}")
    public List<Entry> entry(@PathVariable("state") String state) {
        List<Entry> tE = EntryDatabaseList.repo.findByState(state.toUpperCase());
        return tE;
    }
    @RequestMapping(value="/entries/{state}/*")
    public List<Entry> entryAll(@PathVariable("state") String state) {
        return entry(state);
    }
    @RequestMapping(value="/entries/{state}/{isBill}")
    public List<Entry> entry(@PathVariable("state") String state,
                             @PathVariable("isBill") boolean type) {
        List<Entry> tE = EntryDatabaseList.repo.findByStateAndType(state.toUpperCase(),
                type);
        return tE;
    }

    @RequestMapping(value="/entries")
    public List<Entry> entry() {
        // Rework all of this
        //This right here is meant to display our a visual representation of our entries
        List<Entry> tE = EntryDatabaseList.repo.getAllEntries();
        return tE;
    }
}
