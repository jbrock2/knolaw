package com.knolaw.knolawEntries;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.awt.print.Pageable;
import java.util.List;

public interface EntryRepo extends JpaRepository<Entry, Long> {

    @Query("SELECT ent FROM Entry ent WHERE ent.state = :state")
    List<Entry> findByState(@Param("state") String determineState);

    @Query("SELECT ent FROM Entry ent WHERE ent.state = :state AND ent.isBill = :isBill")
    List<Entry> findByStateAndType(@Param("state") String determineState,
                                   @Param("isBill") boolean determineBill);

    @Query("SELECT ent FROM Entry ent WHERE ent.isBill = :isBill")
    List<Entry> findByOnlyType(@Param("isBill") boolean determineBill);

    @Query("SELECT ent FROM Entry ent WHERE ent.title = :title")
    Entry findEntryByName(@Param("title") String title);

    @Query("SELECT ent FROM Entry ent WHERE ent.url = :url")
    Entry findEntryByURL(@Param("url") String url);

    @Query("SELECT ent FROM Entry ent ORDER BY ent.state")
    List<Entry> getAllEntries();
}
