package com.knolaw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnolawApplication {

	public static void main(String[] args) {
		SpringApplication.run(KnolawApplication.class, args);
	}
}
