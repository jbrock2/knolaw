package com.knolaw.WebScrapping;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import com.knolaw.knolawEntries.Entry;

import java.util.*;

public class WebScrape {
    private static String currentYear;
    private static String scrapeLawURL = "https://legiscan.com/";
    private static String[] scrapeURL = new String[] {"https://abc7.com/place/",
            "http://www.foxnews.com/category/us/us-regions/"};
    private static String[] stateList = {
            "alaska","alabama","arkansas","arizona","california",
            "colorado","connecticut","delaware","florida","georgia",
            "hawaii","iowa","idaho","illinois","indiana","kansas","kentucky","louisiana",
            "massachusetts","maryland","maine","michigan","minnesota","mississippi","montana","missouri",
            "north-carolina","north-dakota","nebraska","new-hampshire",
            "new-jersey","new-mexico","nevada","new-york","ohio","oklahoma",
            "oregon","pennsylvania","rhode-island","south-carolina","south-dakota","tennessee",
            "texas","utah","virginia","vermont", "washington","wisconsin", "west-virginia","wyoming"
    };
    private static String[] stateAbbrList = {
            "AK","AL","AR","AZ","CA","CO", "CT","DE","FL","GA","HI","IA", "ID","IL","IN","KS","KY","LA",
            "MA","MD","ME","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK",
            "OR","PA","RI","SC","SD","TN","TX","UT","VA","VT","WA","WI","WV","WY"
    };

    private static void moreABCEntries(HtmlPage page, List<Entry> storeEntryFind, String state, String scrape) {
        int i = 0;
        while (true) {
            try{
                String isTop = ("//div[@class='headline-list-item item-"+i+" has-image']/");
                //System.out.println("This is our XPath: "+isTop);
                DomAttr d_url = (DomAttr)page.getByXPath(isTop+"a/@href").get(0);
                //System.out.println("This is d_url registered");
                //ArrayList<DomText> d_title = (ArrayList<DomText>)page.getByXPath(isTop+"a/div[@class='headline']").;
                //String title = d_title.get(0).getWholeText();
                String url = d_url.getTextContent();
                String[] getTitle = url.split("/");
                String title = "";
                for (String checkString : getTitle) {
                    if (checkString.contains("-")) {title = checkString.replace('-',' ');}
                }
                storeEntryFind.add(new Entry(title.toUpperCase(),"No Description Found",state.toUpperCase(),scrape+url.substring(1),false,"Not a Law"));
                i++;
            }
            finally { }
        }
    }

    public static List<Entry> scrapeABCEntries(List<Entry> storeEntryFind) {
        WebClient storeClient = new WebClient();
        storeClient.getOptions().setJavaScriptEnabled(false);
        storeClient.getOptions().setCssEnabled(false);
        for (String state : stateList) {
            String connectURL = scrapeURL[0]+state;
            try {
                System.out.println("Connected! State: "+state);
                HtmlPage page = storeClient.getPage(connectURL);
                //this is going to be our main story that we add
                try {
                    DomText d_title = (DomText) page.getByXPath("//div[@class='headline']/text()").get(0);
                    DomText d_desc = (DomText) page.getByXPath("//div[@class='callout']/text()").get(0);
                    String title = d_title.getWholeText();
                    String desc = d_desc.getWholeText();
                    if (desc.length() > 125) {
                        desc = desc.substring(0, 125);
                    }
                    desc += "...";
                    DomAttr d_url = (DomAttr) page.getByXPath("//div[@class='headline-list-item item-0 has-image']/child::a/@href").get(0);
                    String url = d_url.getTextContent();
                    //System.out.println("Title: "+title+" Desc: "+desc+" url: "+url);
                    storeEntryFind.add(new Entry(title.toUpperCase(), desc, state.toUpperCase(), scrapeURL[0] + url.substring(1),false,"Not a Law"));
                    //This is going to be representative of all of the smaller stories that we find
                    //
                    moreABCEntries(page,storeEntryFind, state.toUpperCase(),scrapeURL[0]);
                } catch(Exception e) {System.out.print("Moving on\n");}
            }
            catch(Exception e){
                e.printStackTrace();
                //System.out.println("Testing for Error");
                continue;
            }
        }
        storeClient.close();
        return storeEntryFind;
    }

    public static List<Entry> scrapeLawEntries(List<Entry> storeLawFind){
        WebClient storeLawClient =  new WebClient();
        storeLawClient.getOptions().setJavaScriptEnabled(false);
        storeLawClient.getOptions().setCssEnabled(false);
        for (int index = 0; index < stateAbbrList.length; index++) {
            String connectURL = scrapeLawURL+stateAbbrList[index];
            try {
                HtmlPage page = storeLawClient.getPage(connectURL);
                try {
                    HtmlTableBody bill_TableBody = (HtmlTableBody) page.getByXPath("//table[@class='gaits-browser sticky-enabled']/tbody").get(0);
                    List<HtmlTableRow> bill_Table = bill_TableBody.getRows();
                    for(int i = 0; i < bill_Table.size(); i++) {
                        HtmlTableRow hTR = bill_Table.get(i);
                        System.out.println("Loading");
                        DomAttr hTRLink = (DomAttr)page.getByXPath(hTR.getCanonicalXPath()+"//a/@href").get(0);
                        storeLawFind.add(new Entry(hTR.getCell(0).asText(),hTR.getCell(1).asText(),
                                stateList[index].toUpperCase(), scrapeLawURL+hTRLink.getTextContent().substring(1),
                                true,hTR.getCell(1).asText().substring(12)));
                    }
                }
                catch(Exception e) {
                    System.out.println("Something's gone wrong when grabbing the bills");
                    e.printStackTrace();
                }
            }
            catch(Exception e) {
                e.printStackTrace();
                continue;
            }
        }
        return storeLawFind;
    }

    /*public static List<Entry> scrapeFoxEntries(List<Entry> storeEntryFind) {
        WebClient storeFoxClient =  new WebClient();
        storeFoxClient.getOptions().setJavaScriptEnabled(false);
        storeFoxClient.getOptions().setCssEnabled(false);
        for (int index = 0; index < stateList.length; index++) {
            String connectURL = scrapeURL[1]+stateList[index];
            try {
                HtmlPage page = storeFoxClient.getPage(connectURL);
                try {
                    DomText fox_Title = (DomText) page.getByXPath("//div[@class='info']/child::header/child::h4/child::a/text()").get(0);
                    DomText fox_Body = (DomText) page.getByXPath("//div[@class='info']/child::div/child::p/child::a/text()").get(0);
                    String title = fox_Title.getWholeText();
                    String desc = fox_Body.getWholeText();
                    if (desc.length() > 125) {
                        desc = desc.substring(0, 125);
                    }
                    desc += "...";
                    DomAttr d_url = (DomAttr) page.getByXPath("//div[@class='headline-list-item item-0 has-image']/child::a/@href").get(0);
                    String url = d_url.getTextContent();
                    //System.out.println("Title: "+title+" Desc: "+desc+" url: "+url);
                    storeEntryFind.add(new Entry(title.toUpperCase(), desc, stateList[index].toUpperCase(), scrapeURL[1] + url.substring(1),false,"Not a Law"));
                    //This is going to be representative of all of the smaller stories that we find
                    //
                    moreABCEntries(page,storeEntryFind, stateList[index].toUpperCase(),scrapeURL[0]);
                }
                catch(Exception e) {
                    System.out.println("Something went wrong when grabbing values");
                    e.printStackTrace();
                }
            }
            catch(Exception e) {
                System.out.println("Something's gone wrong with grabbing FOX entries");
                e.printStackTrace();
            }
        }
        return storeEntryFind;
    }*/

    public static List<Entry> scrapeEntries() {
        List<Entry> storeEntryFind = new ArrayList<>();
        scrapeLawEntries(storeEntryFind);
        scrapeABCEntries(storeEntryFind);
        Collections.shuffle(storeEntryFind);
        return storeEntryFind;
    }
}
